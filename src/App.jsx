import { useState } from 'react'
import logo from './logo.svg'
import './App.css'
/**** con hmacSHA512 */
// import sha256 from 'crypto-js/sha256'
// import hmacSHA512 from 'crypto-js/hmac-sha512'
// import Base64 from 'crypto-js/enc-base64'
/**** con hmacSHA512 */
import CryptoJS from 'crypto-js'

function App() {
  /**** con hmacSHA512 */
  // const message = 'hello'
  // const nonce = ''
  // const path = ''
  // const privateKey =
  //   'my_super_secret_key_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho_ho'
  // const hashDigest = sha256(nonce + message)
  // const hmacDigest = Base64.stringify(hmacSHA512(path + hashDigest, privateKey))
  // console.log(hmacDigest)
  /**** con hmacSHA512 */

  /**** con AES */
  let messageTotal = ''
  let messageEncriptado = ''
  let messageNoEncriptado = ''
  // Encrypt
  // function encrypt(message, key) {
  //   const ciphertext = CryptoJS.AES.encrypt(message, key).toString()
  //   messageTotal = ciphertext
  //   return ciphertext
  // }

  // function decrypt(ciphertext, key) {
  //   const plaintext = CryptoJS.AES.decrypt(ciphertext, key)
  //   return plaintext.toString(CryptoJS.enc.Utf8)
  // }
  /** Con AES */

  /**** con AES con CFB */
  function encrypt(message, key) {
    const encrypted = CryptoJS.AES.encrypt(message, key, {
      mode: CryptoJS.mode.CFB,
      padding: CryptoJS.pad.AnsiX923,
    })
    messageTotal = encrypted.toString()

    decrypt(messageTotal, 'clave')
    return encrypted.toString()
  }

  function decrypt(message, key) {
    let desencrypted = CryptoJS.AES.decrypt(message, key, {
      mode: CryptoJS.mode.CFB,
      padding: CryptoJS.pad.AnsiX923,
    })
    messageNoEncriptado = desencrypted.toString(CryptoJS.enc.Utf8)
    console.log(
      '🚀 ~ file: App.jsx ~ line 58 ~ App ~ desencrypted',
      desencrypted.toString(CryptoJS.enc.Utf8)
    )
    return desencrypted.toString(CryptoJS.enc.Utf8)
  }

  // video https://www.youtube.com/watch?v=oLuwaZPPKg0
  /**** con AES con GBC */

  /** Con Cipher Output */
  // let JsonFormatter = {
  //   stringify: function (cipherParams) {
  //     // create json object with ciphertext
  //     var jsonObj = {
  //       ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64),
  //     }
  //     // optionally add iv or salt
  //     if (cipherParams.iv) {
  //       jsonObj.iv = cipherParams.iv.toString()
  //     }
  //     if (cipherParams.salt) {
  //       jsonObj.s = cipherParams.salt.toString()
  //     }
  //     // stringify json object
  //     return JSON.stringify(jsonObj)
  //   },
  //   parse: function (jsonStr) {
  //     // parse json string
  //     var jsonObj = JSON.parse(jsonStr)

  //     // extract ciphertext from json object, and create cipher params object
  //     var cipherParams = CryptoJS.lib.CipherParams.create({
  //       ciphertext: CryptoJS.enc.Base64.parse(jsonObj.ct),
  //     })

  //     // optionally extract iv or salt
  //     if (jsonObj.iv) {
  //       cipherParams.iv = CryptoJS.enc.Hex.parse(jsonObj.iv)
  //     }

  //     if (jsonObj.s) {
  //       cipherParams.salt = CryptoJS.enc.Hex.parse(jsonObj.s)
  //     }
  //     return cipherParams
  //   },
  // }

  // options()
  // function options() {
  //   var encrypted = CryptoJS.AES.encrypt('Message', 'Secret Passphrase', {
  //     format: JsonFormatter,
  //   })
  //   messageEncriptado = encrypted.toString()

  //   var decrypted = CryptoJS.AES.decrypt(encrypted, 'Secret Passphrase', {
  //     format: JsonFormatter,
  //   })
  //   messageNoEncriptado = decrypted.toString(CryptoJS.enc.Utf8)
  //   decrypted.toString(CryptoJS.enc.Utf8)

  //   return encrypted, decrypted
  // }

  /** Con Cipher Output */
  return (
    <div className='App'>
      <h1>HOLA</h1>
      <p>Encriptado: {encrypt('Hola', 'clave')}</p>
      <p>Desencriptado: {messageNoEncriptado}</p>
    </div>
  )
}

export default App
